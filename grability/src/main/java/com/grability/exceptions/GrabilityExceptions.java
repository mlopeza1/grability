package com.grability.exceptions;

public class GrabilityExceptions extends Exception {

    private static final long serialVersionUID = 1L;

	public GrabilityExceptions(String message) {
		super(message);
	}
}
