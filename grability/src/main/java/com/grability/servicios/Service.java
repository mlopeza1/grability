package com.grability.servicios;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.grability.controlador.Controlador;
import com.grability.modelo.Entrada;



@Stateless
@Path("tienda")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class Service {
	
	@PersistenceContext
	EntityManager em;
	
	private static final Logger LOG = Logger.getLogger(Service.class.getName());
	
	@GET
	public Response inicio() throws IOException {
		Controlador controlador=new Controlador();
		return controlador.enviar();
	}
	
	@POST
	public Response ejecutar(Entrada entrada) throws IOException {
		LOG.log(Level.WARNING, "Llego al servicio POST");
		Controlador controlador=new Controlador();
		return controlador.calcular(entrada,em);
	}
}
