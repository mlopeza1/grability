package com.grability.servicios;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.grability.exceptions.Validacionescc;

/**
 * @author Michel Lopez
 */
@Provider
public class JWTTokenFilter implements ContainerRequestFilter {
	private static final Logger LOG = Logger.getLogger(JWTTokenFilter.class.getName());

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {
		
		Validacionescc validacionescc = new Validacionescc();

        try {
        	String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        	validacionescc.validarToken(token);
		} catch (Exception e) {
			LOG.log(Level.WARNING, "Entro a exception filter");
			requestContext.abortWith(Response.status(401).entity(e.getMessage()).build());
		}
	}
}

