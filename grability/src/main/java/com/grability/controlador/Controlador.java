package com.grability.controlador;

import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response;

import com.grability.calculos.Calculos;
import com.grability.modelo.CaminoDAO;
import com.grability.modelo.Entrada;
import com.grability.modelo.EntradaDAO;
import com.grability.modelo.PecesDAO;
import com.grability.modelo.TiendaDAO;
import com.grability.modelo.TipoPeces;

public class Controlador {
	public Controlador() {
	}

	public Entrada crear(){
		TipoPeces[] arraytp=new TipoPeces[5];
		arraytp[0]=new TipoPeces(1, new int[]{1});
		arraytp[1]=new TipoPeces(1, new int[]{2});
		arraytp[2]=new TipoPeces(1, new int[]{3});
		arraytp[3]=new TipoPeces(1, new int[]{4});
		arraytp[4]=new TipoPeces(1, new int[]{5});
		
		CaminoDAO[] arrayCamino=new CaminoDAO[5];
		arrayCamino[0]=new CaminoDAO(1, 2,10);
		arrayCamino[1]=new CaminoDAO(1, 3,10);
		arrayCamino[2]=new CaminoDAO(2, 4,10);
		arrayCamino[3]=new CaminoDAO(3, 5,10);
		arrayCamino[4]=new CaminoDAO(4, 5,10);
			
		Entrada ent = new Entrada();
		ent.setN(5);
		ent.setM(5);
		ent.setK(5);
		ent.setTp(arraytp);
		ent.setCamino(arrayCamino);
		
		return ent;
	}
	
	public Response enviar() {
		try {
			Entrada ent=crear();
			return Response.status(201).entity(ent).build();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.status(500).entity(ex.getMessage()).build();
		}
	}
	
	public Response calcular(Entrada ent,EntityManager em) {
		try {
			
			Calculos cal=new Calculos(ent);
			ent.setRespuesta(cal.ejecutar());
			
			
			EntradaDAO entrada=new EntradaDAO();
			entrada.setN(ent.getN());
			entrada.setM(ent.getM());
			entrada.setK(ent.getK());
			em.persist(entrada);
			
			for (int i=0;i<ent.getM();i++){ 
				CaminoDAO caminos=new CaminoDAO();
				caminos.setX(ent.getCamino()[i].getX());
				caminos.setY(ent.getCamino()[i].getY());
				caminos.setZ(ent.getCamino()[i].getZ());
				caminos.setEntradaDAO(entrada);
				em.persist(caminos);
			}
			
			for (int i=0;i<ent.getN();i++){
				TiendaDAO tiendas =new TiendaDAO();
     			tiendas.setX(i+1);
     			tiendas.setCantidad(ent.getTp()[i].getCantTipos());
     			tiendas.setEntradaDAO(entrada);
     			em.persist(tiendas);
				for	(int j=0;j<tiendas.getCantidad();j++){
					PecesDAO peces=new PecesDAO();
					peces.setX(ent.getTp()[i].getTipos()[j]);
					peces.setTiendaDAO(tiendas);
					em.persist(peces);
				}
			}
			return Response.status(201).entity(ent).build();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.status(500).entity(ex.getMessage()).build();
		}
	}
}
