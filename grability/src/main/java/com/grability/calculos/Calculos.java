package com.grability.calculos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;

import com.grability.modelo.Entrada;


public class Calculos {
	private Entrada ent;
	private static int shortest_path[][];
	private static int matrix[][];
	private static int maxn_cities;
	private static int maxn_roads;
	private static int maxn_fish_types_no;
	private static int n_roads;
	private static int shops[];

	public Calculos() {
	}
	
	public Calculos(Entrada ent) {
		this.ent = ent;
	}
	/**
	 * Cramos en grafo
	 *
	 * @param nn_cities
	 * @param nn_roads
	 */
	public static void crearGrafo(int nn_cities, int nn_roads, int nn_fish_types_no) {
		maxn_cities = nn_cities;
		maxn_roads = nn_roads;
		maxn_fish_types_no=nn_fish_types_no;
		n_roads = 0;
		matrix = new int[maxn_cities][maxn_cities];
		shops = new int [maxn_cities];
		shortest_path=new int[maxn_cities][maxn_fish_types_no];
	}

	/**
	 * Insertar arista
	 *
	 * @param v1
	 *            vertice 1
	 * @param v2
	 *            vertice 2
	 * @param dist
	 *            distancia entre cada vertice
	 */
	public static void insertaArista(int v1, int v2, int dist) {
		if (v1 >= maxn_cities || v2 >= maxn_cities) {
			throw new ArrayIndexOutOfBoundsException(
					"n_cities inválidos, fuera de rango"
							+ "nRango de n_cities: 0 - " + (maxn_cities - 1));
		} else if (n_roads == maxn_roads) {
			throw new UnsupportedOperationException(
					"No se puede añadir más n_roads");
		} else {
			matrix[v1][v2] = dist;
			matrix[v2][v1] = dist;
		}
	}
	
	public static void ClearStates() {
//	    memset(shops, 0, sizeof(shops));
	    for (int i = 0; i < maxn_cities; i++) {
	        for (int j = 0; j < maxn_fish_types_no; j++) {
	            shortest_path[i][j] = 1200000000 / 2;
	        }
	    }
	}
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String temp[] = br.readLine().split(" ");
		int n_cities = Integer.parseInt(temp[0]);
		int n_roads = Integer.parseInt(temp[1]);
		int n_fish_types_no = Integer.parseInt(temp[2]);
		
		// crea el grafo y lo llena
		crearGrafo(n_cities, n_roads,n_fish_types_no);
		ClearStates();
		
		
		for (int i = 0; i < n_cities; i++) {
			String temp2[] = br.readLine().split(" ");
			int cantTP=Integer.parseInt(temp2[0]);
			int j=1;
			while (j<=cantTP){
				shops[i]=Integer.parseInt(temp2[j])-1;
				j=j+1;
			}
		}
		
		for (int i = 0; i < n_cities; i++) {
			String temp3[] = br.readLine().split(" ");
			insertaArista(Integer.parseInt(temp3[0])-1,Integer.parseInt(temp3[1])-1, Integer.parseInt(temp3[2]));
		}

		// en este caso calcula la distancia minima entre el nodo 0 y el 4
		int res[]=dijkistra(0);
		int minPath=1200000000;
		for(int i=0;i<n_roads;++i){
			for(int j = i; j<n_roads;++j){
				if(i==n_roads-1 || j == n_roads-1){
					minPath=Math.min(minPath, Math.max( res[i], res[j]));
				}
			}
		}
		
		System.out.println(minPath);
		
	}
	
	public int ejecutar(){
		int n_cities = ent.getN();
		int n_roads = ent.getM();
		int n_fish_types_no = ent.getK();
		crearGrafo(n_cities, n_roads,n_fish_types_no);
		ClearStates();
		
		
		for (int i = 0; i < n_cities; i++) {
			int cantTP=ent.getTp()[i].getCantTipos();
			int j=0;
			while (j<cantTP){
				shops[i]=ent.getTp()[i].getTipos()[j]-1;
				j=j+1;
			}
		}
		
		for (int i = 0; i < n_cities; i++) {
			insertaArista(ent.getCamino()[i].getX()-1,ent.getCamino()[i].getY()-1, ent.getCamino()[i].getZ());
		}

		// en este caso calcula la distancia minima entre el nodo 0 y el 4
		int res[]=dijkistra(0);
		int minPath=1200000000;
		for(int i=0;i<n_roads;++i){
			for(int j = i; j<n_roads;++j){
				if(i==n_roads-1 || j == n_roads-1){
					minPath=Math.min(minPath, Math.max( res[i], res[j]));
				}
			}
		}
		
		System.out.println(minPath);
		
		return minPath;
	}
	
	/**
	 * Calcula la distancia mas corta.
	 * 
	 * @param inicio
	 *            nodo desde donde se va a iniciar.
	 * @return
	 */
	public static int[] dijkistra(int inicio) {
		int[] distancia = new int[maxn_cities];
		int[] padre = new int[maxn_cities];
		boolean[] visto = new boolean[maxn_cities];
		for (int i = 0; i < maxn_cities; i++) {
			distancia[i] = 1200000000;
			padre[i] = -1;
			visto[i] = false;
		}
		distancia[inicio] = 0;
		PriorityQueue<Integer> pila = new PriorityQueue<>();
		pila.add(distancia[inicio]);
		while (!pila.isEmpty()) {
			int u = pila.poll();
			visto[u] = true;
			for (int i = 0; i < maxn_cities; i++) {
				if (matrix[u][i] != 0) {
					// aqui es donde se debe tratar de editar para que nos
					// incluya el parametro gas que es un arreglo de strings
					if (distancia[i] > distancia[u] + matrix[u][i]) {
						distancia[i] = distancia[u] + matrix[u][i];
						padre[i] = u;
						pila.add(i);
					}
				}
			}
		}
		return distancia;
	}

}