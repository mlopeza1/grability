package com.grability.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PecesDAO {
	
	@Id
	@GeneratedValue
	private long id;
	
	private int x;
	
	@ManyToOne
	private TiendaDAO tiendaDAO;
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public TiendaDAO getTiendaDAO() {
		return tiendaDAO;
	}
	public void setTiendaDAO(TiendaDAO tiendaDAO) {
		this.tiendaDAO = tiendaDAO;
	}
	
	

}
