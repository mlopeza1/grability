package com.grability.modelo;

public class TipoPeces {
	private int cantTipos;
	private int tipos[];
	
	public TipoPeces() {
	}
	public TipoPeces(int cantTipos, int[] tipos) {
		this.cantTipos = cantTipos;
		this.tipos = tipos;
	}
	public int getCantTipos() {
		return cantTipos;
	}
	public void setCantTipos(int cantTipos) {
		this.cantTipos = cantTipos;
	}
	public int[] getTipos() {
		return tipos;
	}
	public void setTipos(int[] tipos) {
		this.tipos = tipos;
	}
}
