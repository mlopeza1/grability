package com.grability.modelo;

public class Entrada {

	private int n;
	private int m;
	private int k;
	private TipoPeces[] tp;
	private CaminoDAO[] camino;
	private int respuesta;

	public Entrada() {
	}

	public Entrada(int n, int m, int k, TipoPeces[] tp, CaminoDAO[] camino,int respuesta) {
		this.n = n;
		this.m = m;
		this.k = k;
		this.tp = tp;
		this.camino = camino;
		this.respuesta=respuesta;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}

	public TipoPeces[] getTp() {
		return tp;
	}

	public void setTp(TipoPeces[] tp) {
		this.tp = tp;
	}

	public CaminoDAO[] getCamino() {
		return camino;
	}

	public void setCamino(CaminoDAO[] camino) {
		this.camino = camino;
	}

	public int getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}
	
	

}
