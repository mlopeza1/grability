package com.grability.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CaminoDAO {
	
	@Id
	@GeneratedValue
	private long id;
	
	private int x;
	private int y;
	private int z;
	
	@ManyToOne
	private EntradaDAO entradaDAO;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EntradaDAO getEntradaDAO() {
		return entradaDAO;
	}

	public void setEntradaDAO(EntradaDAO entradaDAO) {
		this.entradaDAO = entradaDAO;
	}

	public CaminoDAO() {
	}
	
	public CaminoDAO(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}
}
