package com.grability.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class TiendaDAO {
	@Id
	@GeneratedValue
	private long id;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public EntradaDAO getEntradaDAO() {
		return entradaDAO;
	}
	public void setEntradaDAO(EntradaDAO entradaDAO) {
		this.entradaDAO = entradaDAO;
	}
	private int x;
	private int cantidad;
	
	@ManyToOne
	private EntradaDAO entradaDAO;
	
	public int getX() {
		return x;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public void setX(int x) {
		this.x = x;
	}
}
