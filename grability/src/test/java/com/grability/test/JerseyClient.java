package com.grability.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
public class JerseyClient {
	private static final Logger LOG = Logger.getLogger(Jersey2Client.class.getName());
	private static FileInputStream imageInFile;
	public static void main(String[] args) throws IOException {

		try {

			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:9999/api/Almacenar");

			Base64 base64 = new Base64();
		    File file = new File("/home/michel/pseudocarpeta/pseudocarpeta2/enblanco.pdf");
		
			imageInFile = new FileInputStream(file);
			byte[] imageData = new byte[(int) file.length()];
			imageInFile.read(imageData);

			String imagenDataString = base64.encodeToString(imageData);
			LOG.log(Level.WARNING, "Base64 : " + imagenDataString);

			String arch = "{\"fechaCreacion\":\"08/07/2017\","
			        + "\"fechaModificacion\":\"08/07/2017\","
					+ "\"llave\":\"1031123075\","
			        + "\"nombre\":\"pruebaEnvio.pdf\","
					+ "\"tamano\":\"10\","
					+ "\"palabrasClave\":\"palabraClave\","
					+ "\"idUsuario\":\"1031123075\","
					+ "\"b64\":\""	+ imagenDataString + "\"}";

			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, arch);
			
			if (response.getStatus() != 201) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			LOG.log(Level.WARNING, "Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOG.log(Level.WARNING, output);
			
			
			
			webResource = client.resource("http://localhost:9999/api/Almacenar/1031123075%2fpruebaEnvio.pdf");
			
			response = webResource.accept("application/json").get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
				}

				output = response.getEntity(String.class);
				LOG.log(Level.WARNING, "Output from Server .... \n");
				LOG.log(Level.WARNING, output);
				
		} catch (Exception e) {

			e.printStackTrace();

		}

	}
}