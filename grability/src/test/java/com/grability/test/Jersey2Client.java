package com.grability.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.jackson.JacksonFeature;

public class Jersey2Client {
	private static final Logger LOG = Logger.getLogger(Jersey2Client.class.getName());
	private static FileInputStream imageInFile;
	public static void main(String[] args) throws IOException {
		
		try {

			Client client = ClientBuilder.newBuilder().register(JacksonFeature.class).build();
			String req = "{\"title\":\"Stripped\", \"singer\":\"Rammstein\"}";
			
			WebTarget target3 = client.target("http://localhost:9999/api/").path("Almacenar/validate");
			String res3 = target3.request().post(Entity.entity(req, MediaType.APPLICATION_JSON), String.class);
			LOG.log(Level.WARNING, res3);

			Base64 base64 = new Base64();
			File file = new File("D:/pseudocarpeta/pseudocarpeta2/pseudocarpeta3/enblanco.pdf");
			imageInFile = new FileInputStream(file);
			byte[] imageData = new byte[(int) file.length()];
			imageInFile.read(imageData);

			String imagenDataString = base64.encodeToString(imageData);
			LOG.log(Level.WARNING, "b64 : " + imagenDataString);
		
			target3 = client.target("http://localhost:9999/api/").path("Almacenar/1031123075%2fpruebaEnvio1000.pdf");
			res3 = target3.request(MediaType.APPLICATION_JSON).get().readEntity(String.class);
			LOG.log(Level.WARNING, res3);

			target3 = client.target("http://localhost:9999/api/").path("Almacenar/buscarTodos/5000");
			String archivos = target3.request(MediaType.APPLICATION_JSON).get().readEntity(String.class);
			LOG.log(Level.WARNING, archivos);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}